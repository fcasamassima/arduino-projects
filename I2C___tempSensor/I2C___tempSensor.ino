/// @dir rtc_demo
/// Hooking up a DS1307 (5V) or DS1340Z (3V) real time clock via I2C.
// 2009-09-17 <jc@wippler.nl> http://opensource.org/licenses/mit-license.php

// the real-time clock is connected to port 1 in I2C mode (AIO = SCK, dIO = SDA)

#include <JeeLib.h>

PortI2C myport (1 ,50);
DeviceI2C tempSens (myport, 0x48);
// this must be added since we're using the watchdog for low-power waiting
ISR(WDT_vect) { Sleepy::watchdogEvent(); }

static void getTemp (byte* buf) {
	tempSens.send();
	tempSens.write(0);	
        tempSens.stop();

    tempSens.receive();
    for (int i=0 ; i< 4; i++){
       buf[i] = tempSens.read(0);
    }

    tempSens.stop();
}

void setup() {
    Serial.begin(57600);
    pinMode(7, OUTPUT);  
    Serial.println("\nTempDemo");

}

void loop() {    
    byte tempreading[10];
    digitalWrite(7, HIGH);
    delay(50);
    getTemp(tempreading);
    while (tempreading[0] == 255){
      Sleepy::loseSomeTime(500);
      getTemp(tempreading);
    }
    digitalWrite(7, LOW);
    Serial.print("temp Registers: ");
    for (byte i = 0; i < 4; ++i) {
        Serial.print(' ');
        Serial.print((int) tempreading[i]);
    }
    Serial.println();
    delay(100);
     Sleepy::loseSomeTime(10000);    

}

#include <JeeLib.h>

#define  channel 7
#define  nodeID 4
typedef struct {
  unsigned char song;
} PayloadItem;
PayloadItem payload;

void setup(void)
{
  Serial.begin(9600);
  
  rf12_initialize(nodeID, RF12_868MHZ, channel);
  Serial.print("Setup done.\n");
}
  
void loop(void){
  
  if(Serial.available() > 0){
    unsigned char ser = Serial.parseInt();
    while (!rf12_canSend())
    rf12_recvDone();
    if (ser > 0 && ser < 120){
      payload.song = ser -1;
      rf12_sendStart(0, &payload, sizeof payload);
      rf12_sendWait(2);
      Serial.print("inviato: ");
      Serial.println(ser);
    }
    else if (ser == 0){}
    else{
      Serial.print("Valore: ");
      Serial.print(ser);
      Serial.println("  non compreso tra 1 e 120");
    }
  }
  
}

/* YourDuino SoftwareSerialExample1
   - Connect to another Arduino running "YD_SoftwareSerialExampleRS485_1Remote"
   - Connect this unit Pins 10, 11, Gnd
   - Pin 3 used for RS485 direction control
   - To other unit Pins 11,10, Gnd  (Cross over)
   - Open Serial Monitor, type in top window. 
   - Should see same characters echoed back from remote Arduino

   Questions: terry@yourduino.com 
*/
#include <RFM69.h>         //get it here: https://www.github.com/lowpowerlab/rfm69
#include <SPI.h>
#include <SPIFlash.h>      //get it here: https://www.github.com/lowpowerlab/spiflash
#include <avr/wdt.h>
#include <WirelessHEX69.h> //get it here: https://github.com/LowPowerLab/WirelessProgramming/tree/master/WirelessHEX69
#include <LowPower.h>

/*-----( Import needed libraries )-----*/
#include <SoftwareSerial.h>
/*-----( Declare Constants and Pin Numbers )-----*/
//#define SSerialRX        10  //Serial Receive pin
//#define SSerialTX        11  //Serial Transmit pin
#define SSerialRX        4   //RS485 Receive pin
#define SSerialTX        5   //RS485 Transmit pin
#define SSerialTxControl 3   //RS485 Direction control

#define RS485Transmit    HIGH
#define RS485Receive     LOW

#define PinLED      9 // Moteino has led on pin 9
#define NODEID      99
#define NETWORKID   5
#define GATEWAYID   1
#define FREQUENCY   RF69_915MHZ //Match this with the version of your Moteino! (others: RF69_433MHZ, RF69_868MHZ)
#define KEY         "yourprivatekey" //has to be same 16 characters/bytes on all nodes, not more not less!
#define ACK_TIME    30  // # of ms to wait for an ack
#define MEAS_DELAY  100


#define DEBUG
//#define EMONCMS_DATATYPE

/*-----( Declare objects )-----*/
SoftwareSerial RS485Serial(SSerialRX, SSerialTX); // RX, TX
SPIFlash flash(8, 0xEF30); //EF40 for 16mbit windbond chip
RFM69 radio;

/*-----( Declare Variables )-----*/
int byteReceived;
int byteSend;
byte sendSize=0;
boolean requestACK = false;
byte VoltAddr    [2] = {0x00, 0x00}; 
byte CurrAddr    [2] = {0x00, 0x06}; 
byte ActPwrAddr  [2] = {0x00, 0x0C};
byte ApparPwrAddr[2] = {0x00, 0x12};
byte ReactPwrAddr[2] = {0x00, 0x18};
byte PfAddr      [2] = {0x00, 0x1E};
byte PhaseAddr   [2] = {0x00, 0x24};
byte FreqAddr    [2] = {0x00, 0x46};
byte TotPwrAddr  [2] = {0x01, 0x56};

#ifdef EMONCMS_DATATYPE
// Structure used to store the data,
// Must be the same on the receiver node
// 16 bit data are used for compttibility with Emoncms
typedef struct {
  unsigned short volt; // Volts are multiplied by 100
  unsigned short amps; //store this nodeId
  unsigned short actPwr; //store this nodeId
  unsigned short apparPwr; //store this nodeId
  unsigned short reactPwr; //store this nodeId
  unsigned short PF; //store this nodeId
  signed   short ph; //the phase should have a sign
  unsigned short freq; //store this nodeId
  unsigned short totPwr; //store this nodeId
} Payload;
Payload theData;
#else
typedef struct {
  float volt; // Volts are multiplied by 100
  float amps; //store this nodeId
  float actPwr; //store this nodeId
  float apparPwr; //store this nodeId
  float reactPwr; //store this nodeId
  float PF; //store this nodeId
  float ph; //store this nodeId
  float freq; //store this nodeId
  float totPwr; //store this nodeId
} Payload;
Payload theData;
#endif

long lastPeriod = -1;
boolean dataReady = false;

void setup()   /****** SETUP: RUNS ONCE ******/
{
  wdt_disable();
   delay(2L * 1000L);
  // Start the built-in serial port, probably to Serial Monitor
  Serial.begin(115200);
  Serial.println("Smart Meter RS485 Interface");  
  Serial.println("Volt \t Curr\t Act Pwr\t Appar Pwr \t Pow Fac\t Phase \tFreq \t PWR");  
  pinMode(PinLED, OUTPUT);   
  pinMode(SSerialTxControl, OUTPUT);   
  digitalWrite(SSerialTxControl, RS485Receive);  // Init Transceiver   
  // Start the software serial port, to another device
  RS485Serial.begin(9600);   // set the data rate 9600 is default for SMD220
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  radio.setHighPower(); //uncomment only for RFM69HW!
  radio.setPowerLevel(10);
  radio.encrypt(KEY);
  if (flash.initialize())
    Serial.println("SPI Flash Init OK!");
  else
    Serial.println("SPI Flash Init FAIL!");
read_value(VoltAddr);
wdt_enable(WDTO_8S);
}//--(end setup )---
byte MeasState = 0;
// byte dataTx[8] = {0x01, 0x04, 0x00, 0x00, 0x00, 0x02, 0x71, 0xCB};
// byte dataRx[9] = {0x01, 0x04, 0x04, 0x43, 0x66, 0x33, 0x34, 0x1B, 0x38};
// byte floatData[4];
#ifdef EMONCMS_DATATYPE
#define CAST100 100*(unsigned short)
#define CAST1 (short)
#else
#define CAST100 
#define CAST1 
#endif
int pause =0;
void loop()   /****** LOOP: RUNS CONSTANTLY ******/
{
  float res = 0;
  if ((int)(millis()/(MEAS_DELAY)) > lastPeriod)
  {
    wdt_reset();
//    Serial.print("Millis and lastp: ");
//    Serial.print(millis());
//    Serial.print(" - ");
//    Serial.println(lastPeriod);
    lastPeriod++;
    MeasState++;
    radio.sleep();
    switch (MeasState){
    case 1: 
    Serial.print("case ");
      pause = 0;
      res = read_value(TotPwrAddr);
      theData.freq = CAST100 res;
      dataReady = true;
    break;
    case 2:
      res = read_value(VoltAddr);
      theData.totPwr = CAST100 res;
      break;
    case 3:
      res = read_value(CurrAddr);
      theData.volt = CAST1 res;
      break;
    case 4:   
      res = read_value(ActPwrAddr);
      theData.amps = CAST1 res;
      break;
   case 5:
     res = read_value(ApparPwrAddr);
     theData.actPwr= CAST100 res;
     break;
   case 6:
     res = read_value(PfAddr);
     theData.apparPwr = CAST1 res;
     break;
   case 7:
     res = read_value(PhaseAddr);
     theData. PF= CAST100 res;
     break;
   case 8:
     res = read_value(FreqAddr);
     theData.ph = CAST1 res;
    break;
   case 9:
    LowPower.powerDown(SLEEP_4S, ADC_OFF, BOD_OFF);  
    MeasState = 0;
    break;
  }
 
 
}
    

  if(dataReady){
    dataReady = false;
    #ifdef DEBUG
    Serial.print("Sending struct (");
    Serial.print(sizeof(theData));
    Serial.print(" bytes) ... ");
    Serial.print(theData.volt);
    Serial.print("\t");
    Serial.print(theData.amps);
    Serial.print("\t");
    Serial.print(theData.actPwr);
    Serial.print("\t");
    Serial.print(theData.apparPwr);
    Serial.print("\t");
    Serial.print(theData.PF);
    Serial.print("\t");
    Serial.print(theData.ph);
    Serial.print("\t");
    Serial.print(theData.freq);
    Serial.print("\t");
    Serial.print(theData.totPwr);
    Serial.print("\t");
    #endif
   if (radio.send(GATEWAYID, (const void*)(&theData), sizeof(theData)),true)
      Serial.print(" ok!");
    else Serial.print(" nothing...");
    Serial.println();
    dataReady = false;
  }
  delay(5);
   // Check for existing RF data, potentially for a new sketch wireless upload
  // For this to work this check has to be done often enough to be
  // picked up when a GATEWAY is trying hard to reach this node for a new sketch wireless upload
 if (radio.receiveDone())
  {
    CheckForWirelessHEX(radio, flash, true);
     if (radio.ACKRequested())
    {
      radio.sendACK();
    }
  }

}//--(end main loop )---


float read_value( byte addr[])
{
  float readVal = 0.0;
  byte dataTx[8] = {0x01, 0x04, 0x00, 0x00, 0x00, 0x02, 0x71, 0xCB};
  byte dataRx[9] = {0x01, 0x04, 0x04, 0x43, 0x66, 0x33, 0x34, 0x1B, 0x38};
  dataTx[2] = addr[0];
  dataTx[3] = addr[1];
  ModRTU_CRC(dataTx,sizeof(dataTx));
 // if (RS485Serial.available())  //Look for data from other Arduino
 // {
    digitalWrite(PinLED, HIGH);  // Show activity
    digitalWrite(SSerialTxControl, RS485Transmit);  // Enable RS485 Transmit   
    for (int i=0; i < 8; i++){
      RS485Serial.write(dataTx[i]);          // Send byte to Smart Meter
    }
 // }
  #ifdef DEBUG
//  for (int i= 0; i< 8; i++){
//    Serial.print(dataTx[i], HEX);        // Show on Serial Monitor
//    Serial.print(" ");
//  }
  #endif
  digitalWrite(PinLED, LOW);  // Show activity    
  delay(10);
  digitalWrite(SSerialTxControl, RS485Receive);  // Disable RS485 Transmit    
  if (RS485Serial.available())  //Look for data from other Arduino
    {
      digitalWrite(PinLED, HIGH);  // Show activity
      for (int i=0; i< 9; i++){
      dataRx[i] = RS485Serial.read();    // Read received byte
    }
    RS485Serial.read();
 //   for (int i= 0; i< 9; i++){
 //   Serial.print(dataRx[i], HEX);        // Show on Serial Monitor
 //   Serial.print(" ");
 //  }
    readVal = byteToFloat(dataRx);
//    Serial.print(readVal);
//    Serial.print("\t");
    delay(10);
    digitalWrite(PinLED, LOW);  // Show activity   
   }  
   return readVal;
}

// converts the response of the SMD220 into a float number
float byteToFloat(byte data[]){
  float snelheid;
  union u_tag {
    byte b[4];
    float fval;
  } u;

  u.b[0] = data[6];
  u.b[1] = data[5];
  u.b[2] = data[4];
  u.b[3] = data[3];

  snelheid = u.fval;
//  for (int i= 0; i< 4; i++){
//    Serial.print(u.b[i], HEX);        // Show on Serial Monitor
  //  Serial.print(" ");
//    }
  
  return snelheid;
}


/* Compute the CRC and stores it in the last two bytes of the buffer
  WARNING buffer must be 2 bytes longer than the len paramenter */
void ModRTU_CRC(byte buf[], int len)
{
  // union is used to convert uint16_t to two bytes
  union u_tag{
    byte crc_b[2];
    unsigned short crc;
  } u_crc;
  
  unsigned short crc = 0xFFFF;
  
  for (int pos = 0; pos < len - 2; pos++) {
    crc ^= (unsigned short)buf[pos];          // XOR byte into least sig. byte of crc
 
    for (int i = 8; i != 0; i--) {    // Loop over each bit
      if ((crc & 0x0001) != 0) {      // If the LSB is set
        crc >>= 1;                    // Shift right and XOR 0xA001
        crc ^= 0xA001;
      }
      else                            // Else LSB is not set
        crc >>= 1;                    // Just shift right
    }
  }
  
  u_crc.crc = crc;
  buf[len -2] = u_crc.crc_b[0];
  buf[len -1] = u_crc.crc_b[1];
  return;  
}
/*-----( Declare User-written Functions )-----*/

//NONE
//*********( THE END )***********


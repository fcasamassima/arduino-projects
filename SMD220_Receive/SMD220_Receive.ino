#include <RFM69.h>
#include <SPI.h>
#include <SPIFlash.h>

#define NODEID      1
#define NETWORKID   5
#define FREQUENCY   RF69_915MHZ //Match this with the version of your Moteino! (others: RF69_433MHZ, RF69_868MHZ)
#define KEY         "yourprivatekey" //has to be same 16 characters/bytes on all nodes, not more not less!
#define LED         9
#define SERIAL_BAUD 115200
#define ACK_TIME    30  // # of ms to wait for an ack

RFM69 radio;
SPIFlash flash(8, 0xEF30); //EF40 for 16mbit windbond chip
bool promiscuousMode = false; //set to 'true' to sniff all packets on the same network
#ifdef EMONCMS
typedef struct {
  unsigned short volt; // Volts are multiplied by 100
  unsigned short amps; //store this nodeId
  unsigned short actPwr; //store this nodeId
  unsigned short apparPwr; //store this nodeId
  unsigned short reactPwr; //store this nodeId
  unsigned short PF; //store this nodeId
  signed   short ph; //the phase should have a sign
  unsigned short freq; //store this nodeId
  unsigned short totPwr; //store this nodeId
} Payload;
#else
typedef struct {
  float volt; // Volts are multiplied by 100
  float amps; //store this nodeId
  float actPwr; //store this nodeId
  float apparPwr; //store this nodeId
  float reactPwr; //store this nodeId
  float PF; //store this nodeId
  float ph; //the phase should have a sign
  float freq; //store this nodeId
  float totPwr; //store this nodeId
} Payload;
#endif
Payload theData;

void setup() {
  Serial.begin(SERIAL_BAUD);
  delay(10);
  radio.initialize(FREQUENCY,NODEID,NETWORKID);
  //radio.setHighPower(); //uncomment only for RFM69HW!
  radio.encrypt(KEY);
  radio.promiscuous(promiscuousMode);
  char buff[50];
  sprintf(buff, "\nListening at %d Mhz...", FREQUENCY==RF69_433MHZ ? 433 : FREQUENCY==RF69_868MHZ ? 868 : 915);
  Serial.println(buff);
  if (flash.initialize())
    Serial.println("SPI Flash Init OK!");
  else
    Serial.println("SPI Flash Init FAIL! (is chip present?)");
   
  Serial.print(" Volts,");
      Serial.print(" Current,");
      Serial.print(" Active Power,");
      Serial.print(" Apparent Power, ");
      Serial.print(" Reactive Power, ");
      Serial.print(" Power Factor, ");
      Serial.print(" Phase, ");
      Serial.print(" Frequency, ");
      Serial.print(" Total Power");
      Serial.println();
}

byte ackCount=0;
void loop() {
  //process any serial input

  if (radio.receiveDone())
  {
    Serial.print('[');Serial.print(radio.SENDERID, DEC);Serial.print("] ");
    Serial.print(" [RX_RSSI:");Serial.print(radio.readRSSI());Serial.print("]");
    if (promiscuousMode)
    {
      Serial.print("to [");Serial.print(radio.TARGETID, DEC);Serial.print("] ");
    }

    if (radio.DATALEN != sizeof(Payload) && radio.SENDERID == 99)
      Serial.print("Invalid payload received, not matching Payload struct!");
    else if(radio.DATALEN == sizeof(Payload) && radio.SENDERID == 99)
    {
      theData = *(Payload*)radio.DATA; //assume radio.DATA actually contains our struct and not something else
      Serial.print("<node=");Serial.print(radio.SENDERID);Serial.print("&json={");
      Serial.print("Voltage:");
      Serial.print(theData.volt);
      Serial.print(",Current:");
      Serial.print(theData.amps,4);
      Serial.print(",Active_Power:");
      Serial.print(theData.actPwr);
      Serial.print(",Apparent_powe:");
      Serial.print(theData.apparPwr);
      Serial.print(",Reactive_Pwr:");
      Serial.print(theData.reactPwr);
      Serial.print(",Power_factor:");
      Serial.print(theData.PF,4);
      Serial.print(",Phase:");
      Serial.print(theData.ph,4);
      Serial.print(",Frequency:");
      Serial.print(theData.freq);
      Serial.print(",Total Power:");
      Serial.print(theData.totPwr);
      Serial.print("}>");
    }
    else{
        int rssi = radio.RSSI;
      Serial.print('[');
      Serial.print(radio.SENDERID);Serial.print("] ");
      if (radio.DATALEN > 0)
      {
        for (byte i = 0; i < radio.DATALEN; i++)
          Serial.print((char)radio.DATA[i]);
        Serial.print("   [RSSI:");
        Serial.print(rssi);
        Serial.print("]");
      }
    }
    
    if (radio.ACKRequested())
    {
      byte theNodeID = radio.SENDERID;
      radio.sendACK();
      Serial.print(" - ACK sent.");

      // When a node requests an ACK, respond to the ACK
      // and also send a packet requesting an ACK (every 3rd one only)
      // This way both TX/RX NODE functions are tested on 1 end at the GATEWAY
      if (ackCount++%3==0)
      {
        Serial.print(" Pinging node ");
        Serial.print(theNodeID);
        Serial.print(" - ACK...");
        delay(3); //need this when sending right after reception .. ?
        if (radio.sendWithRetry(theNodeID, "ACK TEST", 8, 0))  // 0 = only 1 attempt, no retries
          Serial.print("ok!");
        else Serial.print("nothing");
      }
    }
    Serial.println();
    Blink(LED,3);
  }
}

void Blink(byte PIN, int DELAY_MS)
{
  pinMode(PIN, OUTPUT);
  digitalWrite(PIN,HIGH);
  delay(DELAY_MS);
  digitalWrite(PIN,LOW);
}


#include <JeeLib.h>
#include <DHT22.h>

//#define DEBUG
#define DHT22_PIN 7

int LDR_Pin = A3; //analog pin 3
int wakePin = 3; // interrupt pin on Jeenode
Port relays (3);

static int sleepyCycles = 0;
volatile int intDetect = 0;

// Setup a DHT22 instance
DHT22 myDHT22(DHT22_PIN);

typedef struct {
  uint8_t PIR_sens;
  int16_t temp,hum,light;
} PayloadItem;
PayloadItem payload;


ISR(WDT_vect) { Sleepy::watchdogEvent(); }

void int_ISR()
{
  intDetect = 1;
 // detachInterrupt(1);
}

void setup(){
  cli();
  attachInterrupt(1, int_ISR, RISING); // Interrupt 1 for Pin 3 (pin 2 is used for radio)
  sei();
  delay(100);
  #ifdef DEBUG
  Serial.begin(9600);
  #else
  rf12_initialize(20, RF12_868MHZ, 6);
  #endif
}

void loop(){

   attachInterrupt(1, int_ISR, RISING);
   Sleepy::loseSomeTime(60000);
   sleepyCycles++;
   
     if (intDetect)
   {
     #ifdef DEBUG
       Serial.print("Int detected \r\n");
     #else
       rf12_sleep(RF12_WAKEUP);
     #endif
     payload.PIR_sens = 1; 
     if(analogRead(LDR_Pin) < 250){
     relays.digiWrite(1);
     }
     read_sensors();
     SendPayload();
     while (digitalRead(wakePin));
     delay(3000);
     //Sleepy::loseSomeTime(3000);
     relays.digiWrite(0);
     payload.PIR_sens = 0;
     intDetect = 0;
    }
 attachInterrupt(1, int_ISR, RISING);
 
 if (sleepyCycles > 10){
     read_sensors();
     SendPayload();
     sleepyCycles = 0;
 }
 attachInterrupt(1, int_ISR, RISING);
}

  void read_sensors(){
  int16_t LDRReading;  
  int16_t  Temp,Hum;
  LDRReading= analogRead(LDR_Pin);
  delay(200);
  DHT22_ERROR_t errorCode;
  errorCode = myDHT22.readData();
  switch(errorCode)
  {
    case DHT_ERROR_NONE:
      Temp = (int16_t)(myDHT22.getTemperatureC() * 100);
      Hum = (int16_t) (myDHT22.getHumidity() * 100);
      payload.temp = Temp;
      payload.hum = Hum;
      payload.light = LDRReading;
      break;
     #ifdef DEBUG
     case DHT_ERROR_CHECKSUM:
      Serial.print("check sum error ");
      Serial.print(myDHT22.getTemperatureC());
      Serial.print("C ");
      Serial.print(myDHT22.getHumidity());
      Serial.println("%");
      break;
    case DHT_BUS_HUNG:
      Serial.println("BUS Hung ");
      break;
    case DHT_ERROR_NOT_PRESENT:
      Serial.println("Not Present ");
      break;
    case DHT_ERROR_ACK_TOO_LONG:
      Serial.println("ACK time out ");
      break;
    case DHT_ERROR_SYNC_TIMEOUT:
      Serial.println("Sync Timeout ");
      break;
    case DHT_ERROR_DATA_TIMEOUT:
      Serial.println("Data Timeout ");
      break;
    case DHT_ERROR_TOOQUICK:
      Serial.println("Polled to quick ");
      break;
    #endif
         
      
  }
 }


#ifdef DEBUG
  void SendPayload(){
    Serial.print(' ');
    Serial.print(payload.temp);
    Serial.print (' ');
    Serial.print(payload.hum);
    Serial.print (' ');
    Serial.print(payload.light);
    Serial.print (' ');
    Serial.print(payload.PIR_sens);
    Serial.println();
    delay(100);
  }
#else
  void SendPayload(){
       while (!rf12_canSend())
       rf12_recvDone();
       rf12_sendStart(0, &payload, sizeof payload);
       rf12_sendWait(2);
       rf12_sleep(RF12_SLEEP);
       attachInterrupt(1, int_ISR, FALLING);
  }
#endif 

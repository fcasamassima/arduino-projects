
#include <avr/sleep.h>
#include <JeeLib.h>

int wakePin = 3;
int ledPin = 13;
static long payload = 0;
volatile int intDetect = 0;

void int_ISR()
{
  intDetect = 1;
}

void setup()
{
  pinMode(ledPin, OUTPUT);
  pinMode(wakePin, INPUT);
  digitalWrite(wakePin, HIGH);       // turn on pullup resistors
  
  attachInterrupt(1, int_ISR, FALLING); // Interrupt 1 for Pin 3 (pin 2 is used for radio)
  rf12_initialize(21, RF12_868MHZ, 5);
}

void loop()
{
   set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here
   sleep_enable();          // enables the sleep bit in the mcucr register
   attachInterrupt(1, int_ISR, FALLING);
   rf12_sleep(RF12_SLEEP);
   sleep_mode();            // here the device is actually 
   
   if (intDetect)
   {
     rf12_sleep(RF12_WAKEUP);
     ++payload; 
     while (!rf12_canSend())
     rf12_recvDone();
     rf12_sendStart(0, &payload, sizeof payload);
     rf12_sendWait(2);
     rf12_sleep(RF12_SLEEP);
     intDetect = 0;
    }
}  
  

/*
  DigitalReadSerial
 Reads a digital input on pin 2, prints the result to the serial monitor 
 
 This example code is in the public domain.
 */

// digital pin 2 has a pushbutton attached to it. Give it a name:
int pushButton = 3;
int buttonPwr = 2;
int ledPin = 9;

int ledStripState = 0;
int buttonState;             // the current reading from the input pin
int lastButtonState = LOW;   // the previous reading from the input pin

// the following variables are long's because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long lastDebounceTime = 0;  // the last time the output pin was toggled
long debounceDelay = 50;    // the debounce time; increase if the output flickers


// the setup routine runs once when you press reset:
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // make the pushbutton's pin an input:
  pinMode(pushButton, INPUT);
  pinMode(buttonPwr, OUTPUT);
  pinMode(ledPin, OUTPUT);
  analogWrite(ledPin,0);
  digitalWrite(buttonPwr, HIGH);
}
// the loop routine runs over and over again forever:
void loop() {
  // read the input pin:
  
  int reading = digitalRead(pushButton);
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  } 
  
   if ((millis() - lastDebounceTime) > debounceDelay) {
    // whatever the reading is at, it's been there for longer
    // than the debounce delay, so take it as the actual current state:

    // if the button state has changed:
    if (reading != buttonState) {
      buttonState = reading;

      // only toggle the LED if the new button state is HIGH
      if (buttonState == HIGH) {
        ledStripState = ledStripState +1;
        if (ledStripState > 3){
          ledStripState = 0;
        }
      }
    }
  }
  Serial.println(ledStripState);
  
  lastButtonState = reading;  
  switch (ledStripState){
    case 0:
      analogWrite(ledPin, 0);
      return;
   case 1:
      analogWrite(ledPin, 100);
      return;
   case 2:
      analogWrite(ledPin, 180);
      return;  
   case 3:
      analogWrite(ledPin, 254);
      return;
  default: 
    return;      
  }
  // print out the state of the button:
 

delay(20);


}





#include <avr/sleep.h>
#include <JeeLib.h>
//#define DEBUG
#define SensorPowerPin 7
//#include <EEPROM.h>

int wakePin = 3;
int ledPin = 13;
PortI2C myport (1 ,50);
DeviceI2C tempSens (myport, 0x48);


static int sleepyCycles = 0;
volatile int intDetect = 0;
typedef struct {
  int temp;
} PayloadItem;
PayloadItem payload;

ISR(WDT_vect) { Sleepy::watchdogEvent(); }

void int_ISR()
{
  intDetect = 1;
 // detachInterrupt(1);
}

#ifdef DEBUG
  void SendPayload(){
    Serial.print(' ');
    Serial.print(payload.temp);
    Serial.println();
    delay(100);
  }
#else
  void SendPayload(){
       while (!rf12_canSend())
       rf12_recvDone();
       rf12_sendStart(0, &payload, sizeof payload);
       rf12_sendWait(2);
       rf12_sleep(RF12_SLEEP);
       attachInterrupt(1, int_ISR, FALLING);
  }
#endif 
int GetTemp(){
    byte buf[3];
    int temp = 0;
    tempSens.send();
    tempSens.write(0);	
    tempSens.stop();
    tempSens.receive();
    for (int i=0 ; i< 3; i++){
         buf[i] = tempSens.read(0);
    }
    tempSens.stop();
    Serial.print("Got Temp");
    temp = buf[0];
    temp = temp<<8 | buf[1];
    return temp;
}
void setup()
{
//  byte eepromval[4];
  cli();
  pinMode(ledPin, OUTPUT);
  pinMode(wakePin, INPUT);
  pinMode(SensorPowerPin, OUTPUT);   //Digital pin port 2
  digitalWrite(wakePin, HIGH);       // turn on pullup resistors
  attachInterrupt(1, int_ISR, FALLING); // Interrupt 1 for Pin 3 (pin 2 is used for radio)
  sei();
  #ifdef DEBUG
    Serial.begin(57600);
    Serial.println("\nTempDemo");
  #else  
    rf12_initialize(22, RF12_868MHZ, 6);
    rf12_control(0xC040); // set low-battery level to 2.2V i.s.o. 3.1V
  #endif
  delay(100);
}

void loop()
{
//   set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here
//   sleep_enable();          // enables the sleep bit in the mcucr register
//   
//   rf12_sleep(RF12_SLEEP);
//   sleep_mode();            // here the device is actually 
   //Sleepy::powerDown ();
   Sleepy::loseSomeTime(60000);
   sleepyCycles++;
    
   if (sleepyCycles > 1){
     byte timeout = 0;
     digitalWrite(SensorPowerPin, HIGH);
     delay(50);
     Serial.print("Getting Temp \r\n");
     payload.temp = GetTemp();
     while (payload.temp > 32000){
         timeout++;
        Sleepy::loseSomeTime(500);
        payload.temp = GetTemp();
        if (timeout >20){
          timeout = 0;
          payload.temp = 0xFFFF;
          break;
        }
    }
     digitalWrite(SensorPowerPin, LOW);
     SendPayload();
    // payload.temp = 0;
     sleepyCycles = 0;
   }


    attachInterrupt(1, int_ISR, FALLING);
}  
  


#include <avr/sleep.h>
#include <JeeLib.h>
//#include <EEPROM.h>

int wakePin = 3;
int ledPin = 13;
static long payload = 713;
static int sleepyCycles = 0;
volatile int intDetect = 0;

ISR(WDT_vect) { Sleepy::watchdogEvent(); }

void int_ISR()
{
  intDetect = 1;
 // detachInterrupt(1);
}

void SendPayload(){
     while (!rf12_canSend())
     rf12_recvDone();
     rf12_sendStart(0, &payload, sizeof payload);
     rf12_sendWait(2);
     rf12_sleep(RF12_SLEEP);
     attachInterrupt(1, int_ISR, FALLING);
}
void setup()
{
//  byte eepromval[4];
  cli();
  pinMode(ledPin, OUTPUT);
  pinMode(wakePin, INPUT);
  digitalWrite(wakePin, HIGH);       // turn on pullup resistors
//  for (int i = 0; i<4; i++){
//    eepromval[i] = EEPROM.read(i);
//  }
  attachInterrupt(1, int_ISR, FALLING); // Interrupt 1 for Pin 3 (pin 2 is used for radio)
   sei();
  rf12_initialize(21, RF12_868MHZ, 6);
  
  rf12_control(0xC040); // set low-battery level to 2.2V i.s.o. 3.1V
  delay(100);
}

void loop()
{
//   set_sleep_mode(SLEEP_MODE_PWR_DOWN);   // sleep mode is set here
//   sleep_enable();          // enables the sleep bit in the mcucr register
//   
//   rf12_sleep(RF12_SLEEP);
//   sleep_mode();            // here the device is actually 
   //Sleepy::powerDown ();
   Sleepy::loseSomeTime(60000);
   sleepyCycles++;
   if (intDetect)
   {
     rf12_sleep(RF12_WAKEUP);
     ++payload; 
     SendPayload();
     intDetect = 0;
    }
    
   if (sleepyCycles > 10){
     SendPayload();
     sleepyCycles = 0;
   }


  //  attachInterrupt(1, int_ISR, FALLING);
}  
  

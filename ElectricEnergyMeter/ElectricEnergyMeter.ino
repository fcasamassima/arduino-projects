#include <avr/sleep.h>
#include <JeeLib.h>
//#include <EEPROM.h>

int wakePin = 3;
int ledPin = 13;
static long payload = 2001;
static long pulsecounter = 0
long old_pulsecounter = 0;
volatile int intDetect = 0;
static int sleepyCycles = 0;

void int_ISR()
{
  intDetect = 1;
 // detachInterrupt(1);
}

void setup()
{
//  byte eepromval[4];
  //pinMode(ledPin, OUTPUT);
  pinMode(wakePin, INPUT);
  digitalWrite(wakePin, HIGH);       // turn on pullup resistors

  attachInterrupt(1, int_ISR, FALLING); // Interrupt 1 for Pin 3 (pin 2 is used for radio)
  rf12_initialize(19, RF12_868MHZ, 6);
  
  rf12_control(0xC040); // set low-battery level to 2.2V i.s.o. 3.1V
  delay(10);
  old_pulsecounter = 0;
}

void loop()
{
   Sleepy::loseSomeTime(60000);
   sleepyCycles++;
   
   if (intDetect)
   {
     ++pulsecounter;
     if (pulsecounter % 50 ==0){
        ++payload; 
       rf12_sleep(RF12_WAKEUP);
       while (!rf12_canSend())
       rf12_recvDone();
       rf12_sendStart(0, &payload, sizeof payload);
       rf12_sendWait(2);
       rf12_sleep(RF12_SLEEP);
     }
     attachInterrupt(1, int_ISR, FALLING);
     intDetect = 0;
   }
   
   if (sleepyCycles > 6){
     unsigned short int watt= (pulsecounter - old_pulsecounter) *10;
     old_pulsecounter = pulsecounter;
     sleepyCycles=0;
   }
    attachInterrupt(1, int_ISR, FALLING);
}  
  

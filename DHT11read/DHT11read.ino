//--------------------------------------------------------------------------------------
// TempTX-tiny ATtiny84 Based Wireless Temperature Sensor Node

// GNU GPL V3
//--------------------------------------------------------------------------------------



#include <JeeLib.h> // https://github.com/jcw/jeelib
#include "pins_arduino.h"
#include <DHT11.h>

ISR(WDT_vect) { Sleepy::watchdogEvent(); } // interrupt handler for JeeLabs Sleepy power saving

#define myNodeID 20      // RF12 node ID in the range 1-30
#define network 5      // RF12 Network group
#define freq RF12_868MHZ // Frequency of RFM12B module

//#define tempPower 10      // DHT Power pin is connected on pin 
#define PowerPin 7      // DHT Power pin is connected on pin 
#define DHT11PIN 3      // DHT Data pin
#define LedPin 10

dht11 DHT11s;
//#define DHT11PIN 0



/*
                     +-\/-+
               VCC  1|    |14  GND
          (D0) PB0  2|    |13  AREF (D10)
          (D1) PB1  3|    |12  PA1 (D9)
             RESET  4|    |11  PA2 (D8)
INT0  PWM (D2) PB2  5|    |10  PA3 (D7)
      PWM (D3) PA7  6|    |9   PA4 (D6)
      PWM (D4) PA6  7|    |8   PA5 (D5) PWM
                     +----+
*/


int tempReading;         // Analogue reading from the sensor

//########################################################################################################################
//Data Structure to be sent
//########################################################################################################################

 typedef struct {
  	  int temp;	// Temperature reading
  	  int temp2;	// Actually humidity reading 
 } Payload;

 Payload temptx;

//########################################################################################################################

void setup() {
  pinMode(LedPin, OUTPUT);
  digitalWrite(LedPin,LOW);    
  Sleepy::loseSomeTime(1024); 
  digitalWrite(LedPin,HIGH);
  rf12_initialize(myNodeID,freq,network); // Initialize RFM12 with settings defined above 
  rf12_control(0xC040);
  rf12_sleep(0);                          // Put the RFM12 to sleep
  analogReference(INTERNAL);  // Set the aref to the internal 1.1V reference
  pinMode(PowerPin, OUTPUT); // set power pin for DHT11 to output
  digitalWrite(LedPin, HIGH);
}

void loop() {
  
  bitClear(PRR, PRADC); // power up the ADC
  ADCSRA |= bit(ADEN); // enable the ADC  
  
  digitalWrite(PowerPin, HIGH); // turn DHT11 sensor on
  digitalWrite(LedPin, LOW);
  Sleepy::loseSomeTime(100);
  digitalWrite(LedPin, HIGH);
  // The DHT11 requires some time to wake up
  Sleepy::loseSomeTime(1200); 

  int chk = DHT11s.read(DHT11PIN);  //DHT11 doesn't support the decimal part of the reading, only the int part...
  if(chk==DHTLIB_OK) {
  temptx.temp = DHT11s.temperature * 100; // Convert temperature to an integer, reversed at receiving end
  temptx.temp2 = DHT11s.humidity * 100; // Convert temperature to an integer, reversed at receiving end
  }

  digitalWrite(PowerPin, LOW); // turn DHT11 sensor off

  
//  pinMode(tempPower, INPUT); // set power pin for TMP36 to input before sleeping, saves power
//  pinMode(DHT11PIN, OUTPUT);
//  digitalWrite(DHT11PIN, HIGH); // turn DHT11 sensor on
//  pinMode(DHT11PIN, INPUT);
//  digitalWrite(DHT11PIN, HIGH); // turn DHT11 sensor on
//  digitalWrite(tempPower, HIGH); // enable pull-ups
  

  ADCSRA &= ~ bit(ADEN); // disable the ADC
  bitSet(PRR, PRADC); // power down the ADC

  rfwrite(); // Send data via RF 

  for(int j = 0; j < 1; j++) {    // Sleep for 5 minutes
    Sleepy::loseSomeTime(6000); //JeeLabs power save function: enter low power mode for 60 seconds (valid range 16-65000 ms)
  }

}

//--------------------------------------------------------------------------------------------------
// Send payload data via RF
//--------------------------------------------------------------------------------------------------
 static void rfwrite(){

   bitClear(PRR, PRUSI); // enable USI h/w
   rf12_sleep(-1);     //wake up RF module
   while (!rf12_canSend())
   rf12_recvDone();
   rf12_sendStart(0, &temptx, sizeof temptx); 
   rf12_sendWait(2);    //wait for RF to finish sending while in standby mode
   rf12_sleep(0);    //put RF module to sleep
   bitSet(PRR, PRUSI); // disable USI h/w

}
